module.exports = function (app) {
  db = require('./../../../config/db');

  app.get('/api-chats/:currentUserId/:emp_id', function (req, res) {
    var emp_id = req.params.emp_id;
    var currentUserId = req.params.currentUserId;
    var chatQuery = "SELECT id,from_id,to_id,in_date,messages from employee_chat" +
        " where from_id IN ( '" + req.params.emp_id + "', '" + req.params.currentUserId + "' )" +
        " and to_id in ( '" + req.params.emp_id + "', '" + req.params.currentUserId + "') " +
        "order by in_date";

    db.query(chatQuery, function (err, rows) {
      if (err) throw err;
      else {
        var data = {
          chat: rows
        };
        res.send(data);
      }
    });
  });
};