module.exports = function(app){

  db = require('./../../../config/db');


  app.get('/api-personal-info/:userId', function (req, res) {
    var datafromdb = "SELECT * from employee_personal_infos where emp_id = '" + req.params.userId + "' ";

    db.query(datafromdb, function(err, rows){
      if(err) throw err;
      else {
        var data = {
          personalDetails : rows
        };
        res.send(data);
      }
    });
  });

  app.post('/api-personal-info/:userId', function (req, res) {
    var data ={
      emp_id : req.params.userId,
      employee_title : req.body.title,
      employee_name : req.body.employee_name,
      birthday : req.body.birthday,
      gender : req.body.gender,
      blood_group : req.body.blood_group,
      personal_mail_id : req.body.personal_mail_id,
      phone_number : req.body.phone_number,
      present_address : req.body.present_address,
      family_contact_number : req.body.family_contact_number,
      permanent_address : req.body.permanent_address
    };

    var query =
        "SELECT * FROM employee_personal_infos WHERE emp_id = '" + data.emp_id + "' ";
    console.log(query);

    db.query(query,function(err, rows){
      if(err) throw err;
      else {
        if(rows.length) {
          var result = "UPDATE employee_personal_infos SET " +
              "title = '" + data.employee_title + "', " +
              "employee_name = '" + data.employee_name + "', " +
              "birthday = '" + data.birthday + "' ," +
              "gender = '" + data.gender + "', " +
              "blood_group = '" + data.blood_group + "' ," +
              "personal_mail_id = '" + data.personal_mail_id + "' ," +
              "phone_number= '" + data.phone_number + "', " +
              "present_address = '" + data.present_address + "' ," +
              "family_contact_number = '" + data.family_contact_number + "' ," +
              "permanent_address = '" + data.permanent_address + "' " +
              "WHERE emp_id = " + data.emp_id + " ";
          console.log(result,'qry1');
          db.query(result,function(err,rows) {
            if(err) throw err;
            else {
              var response = {
                message : 'success'
              };
              res.send(response);
            }
          });

        } else {
          var insertResult =  "INSERT INTO employee_personal_infos (emp_id,title,employee_name,birthday,gender,blood_group," +
              "personal_mail_id,phone_number,present_address,family_contact_number,permanent_address) VALUES (" +
              "'" + data.emp_id + "', " +
              "'" + data.employee_title + "'," +
              "'" + data.employee_name + "'," +
              "'" + data.birthday + "'," +
              "'" + data.gender + "'," +
              "'" + data.blood_group + "'," +
              "'" + data.personal_mail_id + "'," +
              "'" + data.phone_number + "'," +
              "'" + data.present_address + "'," +
              "'" + data.family_contact_number + "'," +
              "'" + data.permanent_address + "')";
          db.query(insertResult,function(err,rows) {
            if(err) throw err;
            else {
              var response = {
                message : 'success'
              };
              res.send(response);
            }
          });

        }
      }
    });

  });
};