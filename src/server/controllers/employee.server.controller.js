module.exports = function (app) {
var async            = require("async");
  db = require('./../../../config/db');

app.post('/api-employee', function (req, res) {
  var data = {
    employee_name: req.body.employee_name,
      card_id: req.body.card_id,
      office_email_id: req.body.office_email_id,
      password: req.body.password,
      total_sick: req.body.total_sick,
      total_paid: req.body.total_paid
    };

async.waterfall([
    function(callback){ 
var addEmployee = "INSERT INTO employee_login_master (card_id,office_email_id,password,total_sick,total_paid)" +
        "VALUES (" +
        "'" + data.card_id + "' ," +
        "'" + data.office_email_id + "'," +
        "'" + data.password + "'," +
        "'" + data.total_sick + "'," +
        "'" + data.total_paid + "'" + ")";
         

},
  function( lastInId,callback){
      var addEmployeeinfo = "INSERT INTO employee_personal_infos (emp_id,employee_name)" +
        "VALUES (" +
        "'" + lastInId+ "' ," +
        "'" + data.employee_name + "'" + ")";

        console.log(addEmployeeinfo);
            db.query(addEmployeeinfo, function(err, posts) {
                if (err) return callback(err);
               callback(null, 'three');
                 
            });
        // arg1 now equals 'one' and arg2 now equals 'two'
        
    },
    function(arg1, callback){
        // arg1 now equals 'three'
        callback(null, 'done');
    }
], function (err, result) {
   var datas = {
          message: 'success'
        };
        res.send(data);
      }
)});


  app.post('/api-employee-performance/:userId', function (req, res) {

    var data = {
      emp_id: req.params.userId,
      milestone: req.body.milestone,
      productivity: req.body.productivity,
      work_quality: req.body.work_quality,
      attendance: req.body.attendance,
      punctuality: req.body.punctuality,
      creativity: req.body.creativity,
      attitude: req.body.attitude,
      enthusiasm: req.body.enthusiasm,
      team_involvement: req.body.team_involvement,
      work_relations: req.body.work_relations,
      decision_making: req.body.decision_making,
      discipline: req.body.discipline,
      improving_areas: req.body.improving_areas,
      comments: req.body.comments,
      manager_remarks: req.body.manager_remarks
    };
    var addEmployeePerformance = "INSERT INTO employee_performance (emp_id,milestone,productivity,work_quality," +
        "attendance,punctuality,creativity,attitude,enthusiasm,team_involvement,work_relations,decision_making," +
        "discipline,improving_areas,comments,manager_remarks)" +
        "VALUES (" +
        "'" + data.emp_id + "' ," +
        "'" + data.milestone + "'," +
        "'" + data.productivity + "'," +
        "'" + data.work_quality + "'," +
        "'" + data.attendance + "' ," +
        "'" + data.punctuality + "'," +
        "'" + data.creativity + "'," +
        "'" + data.attitude + "'," +
        "'" + data.enthusiasm + "' ," +
        "'" + data.team_involvement + "'," +
        "'" + data.work_relations + "'," +
        "'" + data.decision_making + "'," +
        "'" + data.discipline + "'," +
        "'" + data.improving_areas + "'," +
        "'" + data.comments + "'," +
        "'" + data.manager_remarks + "'" + ")";


    db.query(addEmployeePerformance, function (err, rows) {
      if (err) throw err;
      else {
        var data = {
          message: 'success'
        };
        res.send(data);
      }
    });
  });

  app.get('/api-employee-performance/:userId', function(req, res) {

    var getEmployeePerformance = "SELECT * from employee_performance where emp_id = '" + req.params.userId + "' ";

    db.query(getEmployeePerformance, function(err, rows){
      if(err) throw err;
      else {
        var  data = {
          employeePerformance : rows
        };
        res.send(data);
      }
    });
  });

  app.put('/api-employee-performance/:userId', function (req, res) {

    var data = {
      emp_id: req.params.userId,
      milestone: req.body.milestone,
      productivity: req.body.productivity,
      work_quality: req.body.work_quality,
      attendance: req.body.attendance,
      punctuality: req.body.punctuality,
      creativity: req.body.creativity,
      attitude: req.body.attitude,
      enthusiasm: req.body.enthusiasm,
      team_involvement: req.body.team_involvement,
      work_relations: req.body.work_relations,
      decision_making: req.body.decision_making,
      discipline: req.body.discipline,
      improving_areas: req.body.improving_areas,
      comments: req.body.comments,
      manager_remarks: req.body.manager_remarks
    };

    var updateEmployeePerformance = "UPDATE employee_performance SET " +
        "milestone = '" + data.milestone + "', " +
        "productivity = '" + data.productivity + "', " +
        "work_quality = '" + data.work_quality + "', " +
        "attendance = '" + data.attendance + "', " +
        "punctuality = '" + data.punctuality + "', " +
        "creativity = '" + data.creativity + "', " +
        "attitude = '" + data.attitude + "', " +
        "enthusiasm = '" + data.enthusiasm + "', " +
        "team_involvement = '" + data.team_involvement + "', " +
        "work_relations = '" + data.work_relations + "', " +
        "decision_making = '" + data.decision_making + "', " +
        "discipline = '" + data.discipline + "', " +
        "improving_areas = '" + data.improving_areas + "', " +
        "comments = '" + data.comments + "', " +
        "manager_remarks = '" + data.manager_remarks + "'" +
        "WHERE emp_id = '" + data.emp_id + "'";

    db.query(updateEmployeePerformance, function (err, rows) {
      if (err) throw err;
      else {
        var data = {
          message: 'success'
        };
        res.send(data);
      }
    });
  });
};
