module.exports = function(app){

  db = require('./../../../config/db');
  var datafromdb = 'SELECT * from employee_holidays_list';

  app.get('/api-holidays', function (req, res) {
    db.query(datafromdb, function(err, rows){
      if(err) throw err;
      else {
        var data = {
          holidays : rows
        };
        res.send(data);
      }
    });
  });

  app.post('/api-holidays', function(req,res) {
    var data = {
      holiday_reason : req.body.holiday_reason,
      holiday_date  : req.body.holiday_date
    };

    var addHoliday  = "INSERT INTO employee_holidays_list (holiday,Date)" +
        "VALUES (" +
        "'" + data.holiday_reason + "' ," +
        "'" + data.holiday_date  + "')";

    db.query(addHoliday, function(err, rows){
      if(err) throw err;
      else {
        var response = {
          message : 'success'
        };
        res.send(response);
      }
    });

  });
};
