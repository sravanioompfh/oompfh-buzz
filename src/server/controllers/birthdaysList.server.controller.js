module.exports = function(app) {

  db = require('./../../../config/db');
  app.get('/api-birthdays', function (req, res) {
    var leaveResult = "SELECT employee_name,birthday from employee_personal_infos";

    db.query(leaveResult, function (err, rows) {
      if (err) throw err;
      else {
        var data = {
          leaves: rows
        };
        res.send(data);
      }
    });
  });
};