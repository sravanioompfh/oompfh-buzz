module.exports = function (app) {
  var Q = require('q');

  db = require('./../../../config/db');

  app.get('/api-leaves/:userId', function (req, res) {

    var leaveResult = "SELECT * from employee_leaves where emp_id = '" + req.params.userId + "' ";

    db.query(leaveResult, function (err, rows) {
      if (err) throw err;
      else {
        var data = {
          leaves: rows
        };
        res.send(data);
      }
    });
  });

  app.get('/api-leave-applications', function (req, res) {

    var leaveResult = " SELECT employee_leaves.*,employee_personal_infos.employee_name,employee_leaves.id as leave_id FROM `employee_leaves` join employee_personal_infos on employee_personal_infos.emp_id = employee_leaves.emp_id";

    db.query(leaveResult, function (err, rows) {
      if (err) throw err;
      else {
        var data = {
          leaves: rows
        };
        res.send(data);
      }
    });
  });

  app.post('/api-leaves/:userId', function (req, res) {

    var data = {
      emp_id: req.params.userId,
      leave_type: req.body.leave_type,
      leave_from: req.body.leave_from,
      leave_to: req.body.leave_to,
      leave_description: req.body.leave_description
    };

    var leavePost = "INSERT INTO employee_leaves (emp_id,leave_type,leave_from,leave_to,leave_description)" +
        "VALUES (" +
        "'" + data.emp_id + "' ," +
        "'" + data.leave_type + "'," +
        "'" + data.leave_from + "'," +
        "'" + data.leave_to + "'," +
        "'" + data.leave_description + "')";
    console.log(leavePost);

    db.query(leavePost, function (err, rows) {
      if (err) throw err;
      else {
        var response = {
          message: 'success'
        };
      }
      res.send(response);
    });
  });

  app.put('/api-leaves/:userId/:leaveId/:action', function (req, res) {

    var data = {
      emp_id: req.params.userId,
      leave_id: req.params.leaveId,
      action_type: req.params.action
    };

    if (data.action_type == 'granted' || data.action_type == 'declined') {
      var approveLeave = "UPDATE employee_leaves SET " +
          "leave_status = '" + data.action_type + "' " +
          "WHERE emp_id = " + data.emp_id + " and id = " + data.leave_id;

      db.query(approveLeave, function (err, rows) {
        if (err) throw err;
        else {
          var response = {
            message: 'success'
          };
        }
        res.send(response);
      });
    } else {
      var response = {
        message: 'invalid action'
      };
      res.send(response);
    }
  });

  app.get('/api-employee-leave-tracker',function(req,res) {

    var leaveResult = "select employee_personal_infos.emp_id as emp_log_id,total_paid,total_sick,employee_name  from " +
        "employee_login_master join employee_personal_infos   on employee_login_master.id = employee_personal_infos.emp_id ";
    db.query(leaveResult, function (err, rows) {
      if (err) throw err;
      else {
        var data = {
          leaves: rows
        };
        res.send(data);
      }
    });
  });

  app.get('/api-employee-leave-tracker/:userId',function(req,res){
    var userId = req.params.userId;
    function doQuery1(){
      var defered = Q.defer();
      db.query("select count(id) as paid_leaves_count from employee_leaves where emp_id  = '"+userId+"' and  leave_type='paid'",defered.makeNodeResolver());
      return defered.promise;
    }
    function doQuery2(){
      var defered = Q.defer();
      db.query("select count(id) as sick_leaves_count from employee_leaves where  emp_id  = '"+userId+"' and leave_type='sick'",defered.makeNodeResolver());
      return defered.promise;
    }
    Q.all([doQuery1(),doQuery2()]).then(function(results){
      console.log(results[0][0][0].paid_leaves_count);
      var a  = {paid_leaves_count:results[0][0][0].paid_leaves_count,sick_leaves_count:results[1][0][0].sick_leaves_count};
      res.send(JSON.stringify(a));
      // Hint : your third query would go here
    });
  });

  app.delete('/api-leaves/:userId/:leaveId', function (req, res) {

    var data = {
      emp_id: req.params.userId,
      leave_id: req.params.leaveId
    };

    var leaveDelete =
        "DELETE FROM employee_leaves WHERE emp_id = '" + data.emp_id + "' AND id  = '" + data.leave_id + "';";
    console.log(leaveDelete);

    db.query(leaveDelete, function (err, rows) {
      if (err) throw err;
      else {
        var response = {
          message: 'success'
        };
      }
      res.send(response);
    });
  });

};
