module.exports = function (app) {

    db = require('./../../../config/db');

    app.get('/api-posts/:userId', function (req, res) {

        var getAllPosts = 'SELECT employee_posts.*,employee_personal_infos.employee_name,employee_profiles.photo from '+
            'employee_posts ' + 'join employee_personal_infos on employee_personal_infos.emp_id  =employee_posts.emp_id ' +
            'join employee_profiles on employee_profiles.emp_id  = employee_personal_infos.emp_id order by ' +
            'employee_posts.date DESC';

        db.query(getAllPosts, function (err, rows) {
            if (err) throw err;
            else {
                var data = {
                    posts: rows
                }
                res.send(data);
            }
        });
    });

  var datetime = require('node-datetime');


  app.post('/api-posts/:userId', function(req,res) {
    var data = {
      emp_id  : req.params.userId,
      post_title : req.body.post_title,
      post_body  : req.body.post_body
    }

var dt = datetime.create();
var currentDate = dt.format('Y-m-d H:M:S');

    var addPost  = "INSERT INTO employee_posts (emp_id,post_title,post_body,date)" +
        "VALUES (" +
        "'" + data.emp_id + "' ," +
        "'" + data.post_title + "' ," +
        "'" + data.post_body + "' ," +
        "'" + currentDate+ "')";

    db.query(addPost, function(err, rows){
      if(err) throw err;
      else {
        var response = {
          message : 'success'
        }
        res.send(response);
      }
    });
})
};
