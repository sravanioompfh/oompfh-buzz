var express = require('express')
    ,router = express.Router()
    ,path = require('path');


module.exports = function (app, passport) {
   
   app.post('/api-login',function(req, res){
       passport.authenticate('local',function(err, user){
           if(err) {
               err.status = 'error';
               err.message = 'Username/password invalid';
               res.status(401).send(err);
           } else if (!user) {
               console.log(req.body);
               console.log('e2', user);
               res.status(401).send({
                   status: 'error',
                   message: 'Useranme/password invalid'
               });
           } else {
               res.status(200).send(user);
           }
       })(req,res);
   });


  app.get('/',function (req, res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common',isLoggedIn, function (req, res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/dashboard' ,isLoggedIn,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/profile-edit',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/profile-view',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/birthdays',isLoggedIn,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/planner' ,isLoggedIn,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });


  app.get('/:userId/common/apply-leave',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/leaves',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/notFound' ,isLoggedIn,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/personal-info-view',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/lock-screen',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });


  app.get('/:userId/lock-screen' ,isLoggedIn,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/:userId/common/personal-info-edit',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });


  app.get('/api' ,isLoggedIn,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });

  app.get('/testZone',isLoggedIn ,function(req,res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });
  
  require('./controllers/login.server.controller')(app);
  require('./controllers/personal-info.server.controller')(app);
  require('./controllers/profile.server.controller')(app);
  require('./controllers/upload.server.controller')(app);
  require('./controllers/leaves.server.controller')(app);
  require('./controllers/planner.server.controller')(app);
  require('./controllers/holidaysList.server.controller')(app);
  require('./controllers/birthdaysList.server.controller')(app);
  require('./controllers/lockScreen.server.controller')(app);
  require('./controllers/chat.server.controller')(app);
  require('./controllers/employee.server.controller')(app);
  require('./controllers/newsfeed.server.controller')(app);
  require('./controllers/change-password.server.controller')(app);

  app.get('*',  function (req, res) {
    res.sendFile(path.resolve(__dirname + "/../../" + "index.html"));
  });
  
  app.post('/logout-back',function(req, res) {
                console.log('some thing else');
		req.logout();
		res.redirect('/api-login');
	});
};
function isLoggedIn(req, res, next) {

	if (req.isAuthenticated())
	return next();
        res.redirect('/login-api');
}
