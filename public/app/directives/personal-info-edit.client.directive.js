OompfhBuzz.directive("limitTo", [function () {
  return {
    restrict: "A",
    link: function (scope, elem, attribute) {
      var limit = parseInt(attribute.limitTo);

      angular.element(elem).on("keypress", function (e) {
        if (this.value.length == limit) e.preventDefault();
      });
    }
  }
}]).directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
}).directive('loader', function () {
  return {
    restrict: 'E',
    replace: true,
    template: '<div class="text-center"><i class="fa fa-spinner fa-pulse"></i> Loading Data...</div>'
  };
});