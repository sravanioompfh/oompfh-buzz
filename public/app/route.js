'use strict';

// Setting up route
OompfhBuzz.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$routeProvider',
  function ($stateProvider,
            $urlRouterProvider,
            $routeProvider) {
    $stateProvider.
        state('login', {
          url: '/',
          templateUrl: 'app/views/login.client.view.html',
          controller: 'loginController'
        }).
        state('common', {
          url: '/:userId/common',
          templateUrl: 'app/views/common.client.view.html',
          controller: 'commonController',

        }).
        state('dashboard', {
          url: '/dashboard',
          templateUrl: 'app/views/dashboard.client.view.html',
          parent: 'common',
          controller: 'dashboardController'
        }).
        state('profile-edit', {
          url: '/profile-edit',
          templateUrl: 'app/views/profile-edit.client.view.html',
          parent: 'common',
          controller: 'profileEditController'
        }).
        state('profile-view', {
          url: '/profile-view',
          templateUrl: 'app/views/profile-view.client.view.html',
          parent: 'common',
          controller: 'profileController'
        }).
        state('birthdays', {
          url: '/birthdays',
          templateUrl: 'app/views/birthday.client.view.html',
          controller: 'birthdayController',
          parent: 'common'
        }).
        state('planner', {
          url: '/planner',
          templateUrl: 'app/views/planner.client.view.html',
          controller: 'plannerController',
          parent: 'common'
        }).
        state('apply-leave', {
          url: '/apply-leave',
          templateUrl: 'app/views/apply-leave.client.view.html',
          controller: 'leavesController',
          parent: 'common'
        }).
        state('leaves', {
          url: '/leaves',
          templateUrl: 'app/views/leaves.client.view.html',
          controller: 'leavesController',
          parent: 'common'
        }).
        state('leaves-holidays', {
          url: '/leaves-holidays',
          templateUrl: 'app/views/leaves-holidays.client.view.html',
          parent: 'common',
          controller: 'leavesHolidaysController'
        }).
        state('leaves-statistics', {
          url: '/leaves-statistics',
          templateUrl: 'app/views/leaves-statistics.client.view.html',
          parent: 'common',
          controller: 'leaveStatisticsController'
        }).
        state('notFound', {
          url: '/not-found',
          templateUrl: 'app/views/notfound.client.view.html'
        }).
        state('personal-info-view', {
          url: '/personal-info-view',
          templateUrl: 'app/views/personal-info-view.client.view.html',
          parent: 'common',
          controller: 'viewController'
        }).
        state('lock-screen', {
          url: '/:userId/lock-screen',
          templateUrl: 'app/views/lock-screen.client.view.html',
          controller: 'lockScreenController'
        }).
        state('personal-info-edit', {
          url: '/personal-info-edit',
          templateUrl: 'app/views/personal-info-edit.client.view.html',
          controller: 'editController',
          directive: 'limitTo',
          parent: 'common'
        }).
        state('employees', {
          url: '/employees',
          templateUrl: 'app/views/employees.client.view.html',
          parent: 'common',
          controller: 'photoController',
        }).
        state('employee-add', {
          url: '/employee-add',
          templateUrl: 'app/views/employee-add.client.view.html',
          parent: 'common',
          controller: "employeeAddController"
        }).
        state('api', {
          url: '/api',
          templateUrl: 'app/views/api.client.view.html',
        }).
        state('employee-portfolio', {
          url: '/employee-portfolio/:emp_id',
          templateUrl: 'app/views/employee-portfolio.client.view.html',
          parent: 'common',
          controller: 'photoController'
        }).
        state('employee-performance', {
          url: '/employee-performance/:emp_id',
          templateUrl: 'app/views/employee-performance.client.view.html',
          parent: 'common',
          controller : 'employeePerformanceController'
        }).
        state('employee-portfolio-complete', {
          url: '/employee-portfolio-complete/:emp_id',
          templateUrl: 'app/views/employee-portfolio-complete.client.view.html',
          parent: 'common',
          controller: 'photoController'
        }).
        state('testZone', {
          url: '/testZone',
          templateUrl: 'app/views/test-zone.client.view.html',
          controller: 'testZoneController'
        }).
        state('logout', {
          url: '/',
          templateUrl: 'app/views/login.html',
          controller: 'logoutController'
        }).
        state('cngpwd', {
          url: '/change-password',
          templateUrl: 'app/views/change-password.client.view.html',
          controller: 'changepasswordController',
          parent: 'common'
        });
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/');
  }
]);
