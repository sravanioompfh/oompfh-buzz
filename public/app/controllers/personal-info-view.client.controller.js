OompfhBuzz.controller('viewController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    'Personal',
    '$state',
    '$stateParams',
    function ($scope, $rootScope, $http, $location, Personal, $state, $stateParams) {

        $scope.userid = $stateParams.userId;

        $scope.getPersonalDetails = function () {
            Personal.get({
                    userId: $scope.userid,
                }, {},
                function (data) {
                    $scope.PersonalDetails = data.personalDetails[0];

                    if ($scope.PersonalDetails === undefined) {
                        $scope.show = true;
                        $scope.hide = true;
                    }
                    else {
                        $scope.show = false;
                        $scope.hide = false;
                    }

                    console.log(data, 'Success');

                }, function (data) {
                    console.log(data, 'Error');
                })
        };
        $scope.getPersonalDetails();
    }]);
