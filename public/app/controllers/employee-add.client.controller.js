OompfhBuzz.controller('employeeAddController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    '$state',
    'Notification',
    'EmployeeServer',
    function ($scope, $rootScope, $http, $location,$state,Notification,EmployeeServer) {
        $scope.employeeadddetails = function () {
            EmployeeServer.save ({},
                {
                    employee_name:$scope.employee_name,
                    card_id:$scope.card_id,
                    office_email_id:$scope.office_email_id,
                    password:$scope.password,
                    total_sick:$scope.total_sick,
                    total_paid:$scope.total_paid
                },
                function(data){

                console.log(data,'data is been added');
                    $state.go('employees');
                    Notification.success('Employee has been added successfully!!');
                },
                function(data){
                console.log(data,'error,data not added');
                    Notification.error('Employee is not added');
                });
        }

    }]);
