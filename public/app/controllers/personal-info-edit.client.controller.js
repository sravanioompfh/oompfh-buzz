OompfhBuzz.controller('editController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    'Personal',
    '$state',
    '$stateParams',
    'Notification',
    function ($scope, $rootScope, $http, $location, Personal, $state, $stateParams, Notification) {

        $scope.userid = $stateParams.userId;

        $scope.getPersonalDetails = function () {

            Personal.get({
                    userId: $scope.userid,
                }, {},

                function (data) {
                  $scope.PersonalDetails = data.personalDetails[0];
                  $scope.PersonalDetails.birthday =  new Date($scope.PersonalDetails.birthday);
                  $scope.PersonalDetails = data.personalDetails[0];

                    //$scope.PersonalDetails.birthday = moment($scope.PersonalDetails.birthday).format();
                })
        };

        $scope.savePersonalDetails = function (data) {

            var birth_date = $scope.PersonalDetails.birthday.getUTCFullYear() + '-' + ($scope.PersonalDetails.birthday.getUTCMonth() + 1) +  '-' + ($scope.PersonalDetails.birthday.getUTCDate()+1);

            Personal.save({
                userId: $scope.userid
            }, {
                title: $scope.PersonalDetails.title,
                employee_name: $scope.PersonalDetails.employee_name,
                birthday: birth_date,
                gender: $scope.PersonalDetails.gender,
                blood_group: $scope.PersonalDetails.blood_group,
                personal_mail_id: $scope.PersonalDetails.personal_mail_id,
                phone_number: $scope.PersonalDetails.phone_number,
                present_address: $scope.PersonalDetails.present_address,
                family_contact_number: $scope.PersonalDetails.family_contact_number,
                permanent_address: $scope.PersonalDetails.permanent_address

            }, function (data) {
                console.log(data, 'Success');
                Notification.success('Data has been saved successfully');
                $state.go('personal-info-view');

            }, function (data) {
                console.log(data, 'Error');
            })
        };
    }]);
