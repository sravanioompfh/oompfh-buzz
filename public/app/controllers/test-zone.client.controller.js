OompfhBuzz.controller('testZoneController', ['$scope',
  '$rootScope',
  '$http',
  '$location',
  '$state',
  'Notification',
    'Socket',
  function ($scope, $rootScope, $http, $location,$state,Notification,Socket) {

    $scope.primary = function () {
      Notification('Primary notification Example');
    };

    $scope.info = function () {
      Notification.info('Information notification Example');
    };

    $scope.success = function () {
      Notification.success('Success notification Example');
    };

    $scope.error = function () {
      Notification.error('Error notification Example');
    };

    $scope.warning = function () {
      Notification.warning('Warning notification Example');
    };

    Socket.connect();

    Socket.on("tweet", function(tweet) {

      console.log("tweet from", tweet.user);
      console.log("contents:", tweet.text);
    });

    $scope.dissconnect = function () {

      Socket.disconnect(true);
    };

    $scope.value = 30;
    $scope.options = {
      bgColor: '#2C3E50',
      trackWidth: 50,
      barWidth: 30,
      barColor: '#22baa0',
      textColor: '#eee'
    };

    $scope.myJson1 = {
      type: 'bar',
      "tooltip": {
        "visible": true, //Specify your visibility: true or false.
        text: "%t<br>%v"  //Specify your tooltip text.
      },
      "scale-x": {
        "label":{ /* Scale Title */
          "text":"Here is a category scale",
        },
        //"labels":["Jan","Feb","March","April","May","June","July","Aug"] /* Scale Labels */
      },
      series: [
        {
          values:[20,40,25,50,15,45,33,34]
        }
      ]
    };

  }]);
