OompfhBuzz.controller('leavesHolidaysController', ['$scope',
    '$state',
    'Notification',
    'Leave',
    'Leaves',
    '$stateParams',
    'Holidays',

    function ($scope, $state, Notification, Leave, Leaves, $stateParams, Holidays) {

        $scope.userId = $stateParams.userId;

        $scope.getLeaves = function () {
            Leaves.get({
                userId: $scope.userId

            }, {}, function (data) {
                $scope.leaves = data.leaves;
                console.log($scope.leaves, 'leaves');

            }, function (data) {
                console.log(data, 'error');
            });
        };

        $scope.getLeaves();

        $scope.minDate = new Date();


        $scope.myJson = {
            type: 'pie',
            "tooltip": {
                "visible": true, //Specify your visibility: true or false.
                text: "%t<br>%v"  //Specify your tooltip text.
            },
            series: [
                {values: [10], text: 'paid applied'},
                {values: [12], text: 'AllPaidLeaves'}
            ]
        };

        $scope.myJson1 = {
            type: 'pie',
            "tooltip": {
                "visible": true, //Specify your visibility: true or false.
                text: "%t<br>%v"  //Specify your tooltip text.
            },
            series: [
                {values: [12], text: 'sick applied'},
                {values: [11], text: 'AllSickLeaves'}
            ]
        };


        $scope.getHolidays = function () {
            Holidays.get({}, {},

                function (data) {
                    $scope.holidays = data.holidays;
                    console.log($scope.holidays, 'holidays');

                }, function (data) {
                    console.log(data, 'error');
                });
        };

        $scope.getHolidays();

        $scope.saveHoliday = function (data) {

            var holiday_Date = $scope.Holiday.holiday_date.getUTCFullYear() + '-' + ($scope.Holiday.holiday_date.getUTCMonth() + 1) +  '-' + ($scope.Holiday.holiday_date.getUTCDate()+1);


            Holidays.save({
                userId: $scope.userid
            }, {
                holiday_reason: $scope.Holiday.holiday_reason,
                holiday_date: holiday_Date

            }, function (data) {
                console.log(data, 'Success');
                Notification.success('Holiday has been added successfully');
                $state.go('leaves');

            }, function (data) {
                console.log(data, 'Error');
            })
        };

        $scope.saveStatus = function (type, id, emp_id) {
            $scope.id = id;
            $scope.userid = emp_id;

            if (type == 'granted') {
                $scope.leave_status = 'granted';
            }
            else if (type == 'declined') {
                $scope.leave_status = 'declined';
            }

            Leave.update({
                    userId: $scope.userid,
                    leaveId: $scope.id,
                    action: $scope.leave_status
                }, {},

                function (data) {
                    console.log(data, 'Success');
                    Notification.success('Decision taken');
                    $scope.getLeaves();

                }, function (data) {
                    console.log(data, 'Error');
                })
        };

        $scope.changeOptions = function (taskId) {
            $('.task_'+taskId).hide();
            $('.in_'+taskId).show();

        };

        $scope.editTask = function (taskId) {
            $('.task_'+taskId).show();
            $('.in_'+taskId).hide();
        };

        $scope.closeTask = function (taskId) {
            $('.task_'+taskId).show();
            $('.in_'+taskId).hide();
        };

    }]);
