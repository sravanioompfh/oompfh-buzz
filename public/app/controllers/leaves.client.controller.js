OompfhBuzz.controller('leavesController', ['$scope',
  '$state',
  'Notification',
  'Leave',
  '$stateParams',
  'Holidays',
  '$rootScope',
  function ($scope, $state, Notification, Leave, $stateParams, Holidays, $rootScope) {

    $scope.userId = $stateParams.userId;

    $scope.getLeaves = function () {
      Leave.get({
        userId: $scope.userId
      }, {}, function (data) {
        $scope.leaves = data.leaves;
        var grantedPaidLeavesCount = 0;
        var grantedSickLeavesCount = 0;
        for (i in $scope.leaves) {
          var singleLeave = $scope.leaves[i];
          if (singleLeave.leave_type == 'paid' && singleLeave.leave_status == 'granted') {
            grantedPaidLeavesCount++;
          } else if (singleLeave.leave_type == 'sick' && singleLeave.leave_status == 'granted') {
            grantedSickLeavesCount++;

          }
        }
        $scope.grantedPaidLeavesCount = grantedPaidLeavesCount;
        $scope.grantedSickLeavesCount = grantedSickLeavesCount;

        $scope.initPieCharts($scope.grantedPaidLeavesCount, $scope.grantedSickLeavesCount, $rootScope.loggedUser.total_paid, $rootScope.loggedUser.total_sick);

      }, function (data) {
        console.log(data, 'error');
      });
    };

    $scope.getLeaves();


    $scope.minDate = new Date();


    $scope.initPieCharts = function (paid, sick, totalPaid, totalSick) {
      $scope.myJson = {
        type: 'pie',
        "tooltip": {
          "visible": true, //Specify your visibility: true or false.
          text: "%t<br>%v"  //Specify your tooltip text.
        },
        series: [
          {values: [paid], text: 'paid granted'},
          {values: [totalPaid], text: 'AllPaidLeaves'}
        ]
      };


      $scope.myJson1 = {
        type: 'pie',
        "tooltip": {
          "visible": true, //Specify your visibility: true or false.
          text: "%t<br>%v"  //Specify your tooltip text.
        },
        series: [
          {values: [sick], text: 'sick granted'},
          {values: [totalSick], text: 'AllSickLeaves'}
        ]
      };
    }


    $scope.applyLeave = function () {
      Leave.save({
        userId: $scope.userId
      }, {
        leave_type: $scope.leave_type,
        leave_from: $scope.leave_from,
        leave_to: $scope.leave_to,
        leave_description: $scope.leave_description
      }, function (data) {
        Notification.success('Leave Applied Successfully');
        $state.go('leaves');
        console.log(data, 'frm backend');
      }, function (data) {
        console.log(data, 'error');
      });
    };

    $scope.goToLeavesPage = function () {
      $state.go('leaves');
    };

    $scope.getHolidays = function () {
      Holidays.get({}, {}, function (data) {
        $scope.holidays = data.holidays;
        console.log($scope.holidays, 'holidays');
      }, function (data) {
        console.log(data, 'error');
      });
    };

    $scope.getHolidays();

    $scope.deleteLeave = function (leaveId) {
      Leave.delete({
        userId: $scope.userId,
        leaveId: leaveId
      }, {}, function (data) {
        Notification.success('Leave Deleted Successfully');
        $scope.getLeaves();
      }, function (data) {
        console.log(data, 'error');
      });

    };
  }]);
