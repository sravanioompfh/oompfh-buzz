OompfhBuzz.controller('employeePerformanceController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    '$state',
    'Notification',
    'EmployeePerformance',
    '$stateParams',
    'EmployeePerformanceReport',
    function ($scope, $rootScope, $http, $location, $state, Notification, EmployeePerformance, $stateParams,EmployeePerformanceReport) {


        $scope.options = {
            bgColor: '#2C3E50',
            trackWidth: 50,
            barWidth: 30,
            barColor: '#22baa0',
            textColor: '#eee'
        };
        $scope.userid = $stateParams.userId;
        $scope.getPerformanceDetails = function () {

            EmployeePerformance.get({userId: $scope.userid},
                {},
                function (data) {
                    $scope.employeePerformance=data.employeePerformance;
                    $scope.value1 =$scope.employeePerformance.milestone;
                    $scope.value2 = $scope.employeePerformance.productivity;
                    $scope.value3 =$scope.employeePerformance.work_quality;
                    $scope.value4 = $scope.employeePerformance.attendance;
                    $scope.value5 = $scope.employeePerformance.punctuality;
                    $scope.value6 = $scope.employeePerformance.creativity;
                    $scope.value7 = $scope.employeePerformance.attitude;
                    $scope.value8 =$scope.employeePerformance.enthusiasm;
                    $scope.value9 =$scope.employeePerformance.team_involvement;
                    $scope.value10 = $scope.employeePerformance.work_relations;
                    $scope.value11 = $scope.employeePerformance.decision_making;
                    $scope.value12 =$scope.employeePerformance.discipline;
                    $scope.improvingareas = $scope.employeePerformance.improving_areas;
                    $scope.comments =$scope.employeePerformance.comments;
                    $scope.managerremarks = $scope.employeePerformance.manager_remarks;



                    if ($scope.employeePerformance.length == 0) {
                        $scope.show = true;
                        $scope.hide = true;
                        $scope.hideshow=true;

                    }
                    else  {
                        $scope.show = false;
                        $scope.hide = true;
                        $scope.hideshow=false;
                    }
                    console.log(data, 'data from performance controller');
                },
                function (data) {
                    console.log(data, 'error in performance controller');
                });
        }
        $scope.getPerformanceDetails();




        $scope.saveEmployeePerformanceDetails = function (){
            console.log($scope.value1,'value1----');
            EmployeePerformanceReport.save({userId: $scope.userid
            },{

                    milestone : $scope.value1,
                    productivity : $scope.value2,
                    work_quality : $scope.value3,
                    attendance : $scope.value4,
                    punctuality : $scope.value5,
                    creativity : $scope.value6,
                    attitude : $scope.value7,
                    enthusiasm : $scope.value8,
                    team_involvement : $scope.value9,
                    work_relations : $scope.value10,
                    decision_making : $scope.value11,
                    discipline : $scope.value12,
                    improving_areas :$scope.improvingareas,
                    comments : $scope.comments,
                    manager_remarks : $scope.managerremarks
                },
                function(data){

                    console.log(data,'data is saved in employee performance table');
                    Notification.success('Employee performance is Saved');

                },function(data){
                    console.log(data,'data is not saved');
                    Notification.error('Employee performance is not saved');
                });
        }





        $scope.myJson1 = {
            type: 'bar',
            "tooltip": {
                "visible": true, //Specify your visibility: true or false.
                text: "%t<br>%v"  //Specify your tooltip text.

            },
            "scale-x": {
                "label":{ /* Scale Title */
                    "text":"Here is a category scale",
                },
                "labels":["Milestone","Feb","March","April","May","June","July","Aug"]  /*Scale Labels*/
            },
            series: [
                {
                    values:[$rootScope.value1,40,25,50,15,45,33,34]
                }
            ]
        };


    }]);
