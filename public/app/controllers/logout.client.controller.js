OompfhBuzz.controller('logoutController', ['$scope',
  '$rootScope',
  '$http',
  '$location',
  'Logout',
  'Socket',
  '$state',
  function ($scope, $rootScope, $http, $location,Logout,Socket,$state) {
     
    $scope.logout = function () {
      Logout.get({
      }, {
       
      }, function (data) {
          $scope.setOffline($scope.userId);
          $state.go('login');
      
      }, function (data)
      {
        console.log(data,'error');
      });
    };


     $scope.setOffline  = function(currentUserId){
  console.log(currentUserId,'setOffline function is called');
  Socket.emit('setStatusOffline',{currentUserId:currentUserId});
 };

   
    
  }]);


