OompfhBuzz.controller('photoController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    'Users',
    '$state',
    '$stateParams',
    'Profile',
    'Personal',
    'Planner',
    'Leave',
    function ($scope, $rootScope, $http, $location, Users, $state, $stateParams, Profile, Personal, Planner, Leave) {

        $scope.getUsers = function () {
            Users.get({},
                {},

                function (data) {
                    $scope.photos = data.profile;
                    console.log($scope.photos,'photot controller---pics');
                    console.log(data, 'success in photo function');
                },

                function (data) {
                    console.log(data, 'no Photos Error ');
                })
        };

        $scope.getUsers();

        if ($stateParams.emp_id) {
            console.log($stateParams.emp_id, '$sateparams')

            $scope.getProfileDetails = function () {
                Profile.get({
                    userId: $stateParams.emp_id

                }, {}, function (data) {
                    $scope.details = data.profile[0];
                    console.log(data, 'Success');

                }, function (data) {
                    console.log(data, 'error');
                });
            };

            $scope.getProfileDetails();

            $scope.empid = $stateParams.emp_id;

            $scope.getPersonalDetails = function () {
                Personal.get({
                        userId: $stateParams.emp_id
                    }, {},

                    function (data) {
                        $scope.Person_Details = data.personalDetails[0];
                        console.log(data, 'Success');

                    }, function (data) {
                        console.log(data, 'Error');
                    })
            };
            $scope.getPersonalDetails();

            $scope.fetchOpenTasks = function () {
                Planner.get({
                    userId: $stateParams.emp_id

                }, {}, function (data) {
                    console.log(data);
                    $scope.fetchTasksData = data.fetchOpenTasksData;
                    console.log(data);

                }, function (data) {
                    console.log(data);
                });
            };
            $scope.fetchOpenTasks();

            $scope.getLeaves = function () {
                Leave.get({
                    userId: $stateParams.emp_id

                }, {}, function (data) {
                    $scope.leaves = data.leaves;
                    console.log($scope.leaves, 'leaves');

                }, function (data) {
                    console.log(data, 'error');
                });
            };
            $scope.getLeaves();
        }
    }]);