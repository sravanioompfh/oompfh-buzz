OompfhBuzz.controller('profileController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    'Profile',
    '$state',
    '$stateParams',
    function ($scope, $rootScope, $http, $location, Profile, $state, $stateParams) {
       $scope.userid= $stateParams.userId;

        $scope.getProfileDetails = function () {
            Profile.get({
                    userId: $scope.userid
                },{}, function (data) {

                    console.log(data, 'from backend');
                }, function (data) {
                    console.log(data, 'error');
                });
        };
        $scope.getProfileDetails();
    }]);