OompfhBuzz.controller('profileViewController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    'Profile',
    '$state',
    '$stateParams',
    'Notification',
    function ($scope, $rootScope, $http, $location, Profile, $state, $stateParams, Notification) {
        $scope.userid = $stateParams.userId;
        $scope.getProfileDetails = function () {
            Profile.get({
                userId: $scope.userid
            }, {}, function (data) {
                $scope.details = data.profile[0];
                if ($scope.details == undefined) {
                    $scope.show = true;
                    $scope.hide = true;
                } else {
                    $scope.show = false;
                    $scope.hide = false;
                }
                console.log(data, 'from backend');
            }, function (data) {
                console.log(data, 'error');
            })
        };
        $scope.getProfileDetails();

        $scope.saveProfileDetails = function () {
            Profile.save({
                    userId: $scope.userid
                },
                {   designation: $scope.details.designation,
                    experience: $scope.details.experience,
                    current_project: $scope.details.current_project,
                    past_project: $scope.details.past_project,
                    expertise_skills: $scope.details.expertise_skill,
                    emp_id: $scope.details.emp_id,
                    joining_date: $scope.details.joining_date,
                    skype_id: $scope.details.skype_id,
                    office_mail_id: $scope.details.office_mail_id
                },
                function (data) {
                    console.log(data, 'success');
                    Notification.success('Data has been saved successfully');
                    $state.go('profile-view');
                },
                function (data) {
                    console.log(data, 'error');
                })
        };
    }]);