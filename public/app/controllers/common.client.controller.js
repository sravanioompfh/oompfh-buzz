OompfhBuzz.controller('commonController', ['$scope',
  '$rootScope',
  '$http',
  '$location',
  '$state',
  'Form',
  '$stateParams',
  'Chats',
  'Colleagues',
  'Profile','Personal',
  'Socket',
  function ($scope, $rootScope, $http, $location, $state, Form, $stateParams, Chats, Colleagues, Profile,Personal,Socket) {
    Socket.connect();

    $scope.userId = $stateParams.userId;

    $rootScope.currentUserId = $stateParams.userId;

    $scope.getDetails = function () {
      Form.get({
        userId: $scope.userId
      }, {}, function (data) {
        $scope.user = data.result[0];
        $rootScope.loggedUser = data.result[0];
        console.log($scope.user, 'frm backend');
      }, function (data) {
        console.log(data, 'error');
      });
    }
    $scope.getDetails();

     $scope.userid = $stateParams.userId;
        $scope.getPersonalDetails = function () {

            Personal.get({
                    userId: $scope.userid,
                }, {},
                function (data) {
                  $scope.PersonalDetails = data.personalDetails[0];
                  $scope.PersonalDetails.birthday =  new Date($scope.PersonalDetails.birthday);
                  $scope.PersonalDetails = data.personalDetails[0];

                    //$scope.PersonalDetails.birthday = moment($scope.PersonalDetails.birthday).format();
                })
        };
    $scope.getPersonalDetails();

    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
          $rootScope.tostate = toState;
          $rootScope.fromstate = fromState;
        });

    
    Socket.on("getOnlineusers", function () {
      $scope.getColleagues();
      console.log('get getColleagues list ');
    });


    $scope.getChats = function (emp_id) {
      $scope.loading = true;
      $scope.to_emp_id = emp_id;
      console.log($scope.to_emp_id, 'id');
      Chats.get({
            currentUserId: $rootScope.currentUserId,
            emp_id: $scope.to_emp_id
          }, {},
          function (data) {
            console.log(data.chat, $scope.to_emp_id, $rootScope.currentUserId, 'with chat');
            $scope.loading = false;
            $scope.chatData = data.chat;
          }, function (data) {
            $scope.loading = false;
          });
    };

    $scope.empProfile = function (emp_id) {
      Profile.get({userId: emp_id}, {}, function (data) {
        $scope.userProfileData = data.profile[0];
      }, function (data) {

      });
    };

    //Socket.on("chatRecieved", function (chatRecieved) {
    //  console.log(chatRecieved.data, 'data');
    //  console.log($scope.to_emp_id);
    //  $scope.getChats($scope.to_emp_id);
    //});


    $scope.sendMessage = function (message) {

      Socket.emit("chat", {from_id: $rootScope.currentUserId, to_id: $scope.to_emp_id, messages: message});
      $scope.textMessage = '';
    };

    Socket.on("messageGet", function (messageGet) {
      $scope.getChats($scope.to_emp_id);
      console.log(messageGet.message, 'frm bacekdn', $scope.to_emp_id);
    });

    $scope.getColleagues = function () {
      console.log('clling of get getColleagues');
      Colleagues.get({userId: $rootScope.currentUserId}, {}, function (data) {

        $scope.chatUsers = data.profile;
         console.log($scope.chatUsers,'my styyyy');
      }, function (data) {

      });
    };
    $scope.getColleagues();
    $scope.showWindow = true;
    $scope.currentUser = 0;

    $scope.showUserWindow = function (emp_id) {

      $scope.currentUser = emp_id;
      $scope.showWindow = false;
      $scope.getChats(emp_id);
      $scope.empProfile(emp_id);
      console.log($scope.chatData);
    }
    $scope.closeWindow = function () {

      $scope.showWindow = true;
      $scope.currentUser = 0;

    };




    $scope.time_ago =function(time){

switch (typeof time) {
    case 'number': break;
    case 'string': time = +new Date(time); break;
    case 'object': if (time.constructor === Date) time = time.getTime(); break;
    default: time = +new Date();
}
var time_formats = [
    [60, 'seconds', 1], // 60
    [120, '1 minute ago', '1 minute from now'], // 60*2
    [3600, 'minutes', 60], // 60*60, 60
    [7200, '1 hour ago', '1 hour from now'], // 60*60*2
    [86400, 'hours', 3600], // 60*60*24, 60*60
    [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
    [604800, 'days', 86400], // 60*60*24*7, 60*60*24
    [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
    [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
    [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
    [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
    [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
    [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
    [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
    [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
];
var seconds = (+new Date() - time) / 1000,
    token = 'ago', list_choice = 1;

if (seconds == 0) {
    return 'Just now'
}
if (seconds < 0) {
    seconds = Math.abs(seconds);
    token = 'from now';
    list_choice = 2;
}
var i = 0, format;
while (format = time_formats[i++])
    if (seconds < format[0]) {
        if (typeof format[2] == 'string')
            return format[list_choice];
        else
            return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
    }
return time;
};

  }]);
