OompfhBuzz.controller('leaveStatisticsController', ['$scope',
  '$state',
  'Notification',
  '$stateParams',
  'LeaveTracker',
    '$rootScope',
  function ($scope, $state, Notification, $stateParams, LeaveTracker,$rootScope) {

    $scope.userId = $stateParams.userId;

    $scope.minDate = new Date();

    $scope.changeOptions = function (taskId) {
      $('.task_' + taskId).hide();
      $('.in_' + taskId).show();
    };

    $scope.editTask = function (taskId) {
      $('.task_' + taskId).show();
      $('.in_' + taskId).hide();
    };

    $scope.closeTask = function (taskId) {
      $('.task_' + taskId).show();
      $('.in_' + taskId).hide();
    };

    $scope.getEmployeeLeaveStatus = function (id) {
      LeaveTracker.get({
        userId : id
      },{

      },function (data) {
       $scope.leavesCount = data;
        $scope.fun(data);
      },function (data) {
       console.log('error');
      })

    }
    $rootScope.track = [];
    $scope.fun = function(a){
      $rootScope.track = a;
    }

    $scope.getAllEmployeeTrackingDetails = function () {
      LeaveTracker.get({}, {}, function (data) {
        $scope.employeeLeaveData = data.leaves;
        console.log($scope.employeeLeaveData[0], 'dta');
        angular.forEach($scope.employeeLeaveData, function (value, key) {
          $scope.count = $scope.getEmployeeLeaveStatus($scope.employeeLeaveData[key].emp_log_id);
          console.log($rootScope.track,'llllllllllllllllllllllllll');

        });
      }, function (data) {
        console.log('error');
      })
    }
    $scope.getAllEmployeeTrackingDetails();

  }]);
