OompfhBuzz.controller('dashboardController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    '$state',
    'Notification',
    'Dashboard',
    '$stateParams',
    function ($scope, $rootScope, $http, $location, $state, Notification, Dashboard, $stateParams) {

        $scope.getDashBoard = function () {
            Dashboard.get({
                    userId: $scope.userId
                }, {},

                function (data) {
                    $scope.posts = data.posts;
                    console.log(data, 'success');
                }),

                function (data) {
                    console.log(data, 'error');
                }

        };

        $scope.saveDashBoard = function () {
            Dashboard.save({
                    userId: $scope.userId
                },
                {
                    post_title: $scope.post_title,
                    post_body: $scope.post_body,
                },

                function (data) {
                    console.log(data, 'success');
                    Notification.success('Posted Successfully');
                    document.getElementById("dashboardForm").reset();
                    $scope.getDashBoard();
                },
                function (data) {
                    console.log(data, 'error');
                }
            )
        };


    }]);
