OompfhBuzz.controller('lockScreenController', ['$scope',
    '$rootScope',
    '$http',
    '$location',
    '$state',
    '$stateParams',
    'Notification',
    'Lockscreen',

    function ($scope, $rootScope, $http, $location, $state, $stateParams, Notification, Lockscreen) {
        $scope.userId = $stateParams.userId;
        $scope.getpasswordDetails = function () {
            console.log($scope.password, 'pwd');
            Lockscreen.save({
                    userId: $scope.userId
                },
                {
                    password: $scope.password
                },
                function (data) {
                    if (data.auth == 'true') {
                        console.log($rootScope.fromstate.name);
                        $state.go($rootScope.fromstate.name, {userId: $scope.userId});
                    }
                }, function (data) {
                    console.log(data, 'error');
                });
        };

        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if (!$scope.navigate) {
                    alert('Your session will be expired!! Dont press Back!');
                    event.preventDefault();
                }
            });

    }]);
