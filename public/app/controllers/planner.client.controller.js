OompfhBuzz.controller('plannerController', ['$scope',
  '$rootScope',
  '$http',
  '$location',
  'Planner',

  'Task',
  '$state',
  '$stateParams',
  'Notification',
  function ($scope, $rootScope, $http, $location, Planner,Task, $state, $stateParams, Notification) {

    $scope.userId = $stateParams.userId;

    $scope.savePlanner = function () {

      var creation_date = $scope.data_creation_date.getUTCFullYear() + '-' + ($scope.data_creation_date.getUTCMonth() + 1) +  '-' + ($scope.data_creation_date.getUTCDate()+1);

      Planner.save({
        userId: $scope.userId
      }, {
        task: $scope.task,
        data_creation_date: creation_date
      }, function (data) {
        Notification.success('Task Added Successfully');
        console.log(data, 'frm backend');
        $scope.fetchOpenTasks();
      }, function (data) {
        console.log(data, 'error');
      });
    };

    $scope.fetchOpenTasks = function () {
      Planner.get({
        userId: $scope.userId
      }, {}, function (data) {
        console.log(data);
        $scope.fetchTasksData = data.fetchOpenTasksData;

        console.log(data);
      }, function (data) {
        console.log(data);
      });

    };
    $scope.fetchOpenTasks();

    $scope.deleteTask = function (taskId) {
      console.log(taskId);
      Planner.delete({userId: $scope.userId, taskId: taskId}, {}, function (data) {
        Notification.info('Deleted Successfully');
        $scope.fetchOpenTasks();
      }, function (data) {
      });
    };


    $scope.changeOptions = function (taskId) {
      $('.task_'+taskId).hide();
      $('.in_'+taskId).show();

    };

    $scope.editTask = function (taskId,taskName) {
      $('.task_'+taskId).show();
      $('.in_'+taskId).hide();
      Task.update({
            userId:$scope.userId,
            taskId:taskId
          },{
            task:taskName
          },
          function(data){
            console.log(data,'task is edited');
            $scope.fetchOpenTasks();
            Notification.success('Data is edited successfully!');
          },
          function(data){
            console.log(data,'error in edit');
            Notification.error('Data is not edited');
          });
    };

    $scope.closeTask = function (taskId,taskName) {
      $('.task_'+taskId).show();
      $('.in_'+taskId).hide();

      Task.update(
          { userId:$scope.userId,
            taskId:taskId,
            action:'closed'
          },
          {
            task:taskName
          },
          function(data){
            console.log(data,'task is closed');
              $scope.fetchOpenTasks();
            Notification.success('Task is Closed successfully!');
          },
          function(data){
            console.log(data,'error task is not closed');
            Notification.error('Task is not closed');
          }
      );
    };

    $scope.closeEditTask = function (taskId,taskName) {
      $('.task_'+taskId).show();
      $('.in_'+taskId).hide();

    }
    $scope.openTask = function (taskId,taskName) {
      $('.task_' + taskId).show();
      $('.in_' + taskId).hide();
      Task.update({
            userId:$scope.userId,
            taskId:taskId,
            action:'open'
          },
          {
            task:taskName
          },
          function(data){
            console.log(data,'task is opened');
            $scope.fetchOpenTasks();
            Notification.success('Task is opened successfully!');
          },
          function(data){
            console.log(data,'error task is not opened');
            Notification.error('Task is not opened');
          });
    };

  }]);
