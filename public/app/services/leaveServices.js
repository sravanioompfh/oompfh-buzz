'use strict';

angular.module('OompfhBuzz').factory('Leave', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-leaves/:userId/:leaveId/:action', {}, {
      query: {
        isArray: true
      },
      update: {
        method: 'PUT'
      }
    });
  }
]).factory('Holidays', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-holidays', {}, {
      query: {
        isArray: true
      }
    });
  }

]).factory('Leaves', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-leave-applications', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Planner', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-planner/:userId/:taskId', {}, {});
  }
]).factory('Chats', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-chats/:currentUserId/:emp_id', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Users', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-employee-name-photo', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Colleagues', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-colleagues/:userId', {}, {
      query: {
        isArray: true
      }
    });
  }
]);

