'use strict';

angular.module('OompfhBuzz').factory('Socket', ['socketFactory',
  function (socketFactory) {
    return socketFactory();
  }
])