'use strict';

angular.module('OompfhBuzz').factory('Form', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-login/:userId', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Profile', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-profile/:userId', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Uploadingpic', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + '/:userId/common/profile-view', {}, {
      query: {
        isArray: true
      },
      save: {
        method: "POST",
        headers: {
          "Content-Type": "multipart/form-data;"
        }
      }
    });
  }
]).factory('Personal', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-personal-info/:userId', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Birthday', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-birthdays', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Lockscreen', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-lock-authentication/:userId', {}, {});
  }
]).factory('LeaveTracker', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-employee-leave-tracker/:userId', {}, {});
  }
]).factory('Task', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-planner/:userId/:taskId/:action', {}, {
      query: {
        isArray: true
      },
      update: {
        method: 'PUT'
      }
    });
  }
]).factory('EmployeeServer', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-employee', {}, {
      query: {
        isArray: true
      }
    });
  }
]).factory('Logout', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'logout-back', {}, {
      query: {
        isArray: true
      }

    });
  }
]).factory('ChangePassword', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'changepwd-back/:userId', {}, {});
  }
]).factory('Dashboard', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-posts/:userId', {}, {});
  }
]).factory('EmployeePerformanceReport', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-employee-performance/:userId', {}, {});
  }
]).factory('EmployeePerformance', ['$resource', 'BASE_URL',
  function ($resource, BASE_URL) {
    return $resource(BASE_URL + 'api-employee-performance/:userId', {}, {});
  }
]);
