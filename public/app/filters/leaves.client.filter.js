OompfhBuzz.filter('leaveStatusFilter', function () {
  return function (items, type) {
    if (items) {
      var filtered = [];

      // If resume percentage is with the range
      angular.forEach(items, function (item) {
        if (item.leave_status  == type) {
          filtered.push(item);
        }
      });
      return filtered;
    } else {
      return items;
    }
  };
}).filter('taskFilter',function(){
  return function (items, type) {
    console.log(items,type);
    if (items) {
      var filtered = [];

      // If resume percentage is with the range
      angular.forEach(items, function (item) {
        if (item.status  == type) {
          filtered.push(item);
        }
      });
      return filtered;
    } else {
      return items;
    }
  };
}).filter('inMonthRange',function(){
  return function(items){
    console.log(items,'items is coming');
    if(items){
      var filtered = [];

      angular.forEach(items,function(item){
        var d=new Date(item.birthday);
        var m= d.getMonth()+1;
         var currentDate=new Date();
        var currentMonth=currentDate.getMonth()+1;
        if(m== currentMonth){
          filtered.push(item);
        }
      });
      return filtered;
    }else{
      return items;
    }
  }
});