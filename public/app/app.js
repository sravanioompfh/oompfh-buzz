var OompfhBuzz = angular.module('OompfhBuzz', ['ngRoute',
  'ngResource',
  'ui.router',
  'ngMaterial',
  'ngMessages',
  'ngFileUpload',
  'ui-notification',
  'ngImgCrop',
  'ngFileUpload',
    'ngResource',
    'ui.router',
    'ngMaterial',
    'ngMessages',
    'ui-notification',
    'angularMoment',
    'zingchart-angularjs',
'btford.socket-io',
  'ui.knob']);


angular.element(document).ready(function () {
    //Fixing facebook bug with redirect
    if (window.location.hash === '#_=_') {
        window.location.hash = '#!';
    }
});

// Setting HTML5 Location Mode
angular.module('OompfhBuzz').config([
    '$locationProvider', '$httpProvider', '$urlRouterProvider',
    function ($locationProvider, $httpProvider, $urlRouterProvider) {
        $locationProvider.html5Mode({enabled: true});
    }
]).constant('BASE_URL', 'http://localhost:8585/');