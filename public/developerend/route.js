myApp.config(['$routeProvider',function($routeProvider){
        
    $routeProvider.when('/doc',{
        templateUrl:'app/views/login.html'
    })
    .when('/login-view',{
        templateUrl:'app/views/login-view.html',
        controller : 'loginviewController'
    })
    .when('/profile',{
        templateUrl:'app/views/profile-view.html',
        controller : 'ProfileViewController'
    })
    .when('/logout',{
        templateUrl:'app/views/logout.html',
        controller : 'logoutController'
    })
    .otherwise({
        redirectTo:'/login'
    });
}]);