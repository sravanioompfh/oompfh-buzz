'use strict';

angular.module('myApp').factory('form', ['$resource',
  function ($resource) {
    return $resource('http://localhost:8585/login', {}, {
      query: {
        isArray: true
      }
    });
  }
]);