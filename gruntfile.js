module.exports = function(grunt) {
  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    // configure nodemon
    nodemon: {
      dev: {
        script: 'server.js'
      }
    }
  });

  // load nodemon
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-menu');
  //build task for development environment
  grunt.registerTask('default', ['nodemon']);
};
