var server_port      = require('./environment');
var express          = require('express');
var bodyParser       = require('body-parser');
var json             = require('json');
var expressSession   = require('express-session');
var cookieParser     = require('cookie-parser');
var multer           = require('multer');
var Q                = require('q');
var flash            = require('connect-flash');
var passport         = require('./config/passport');
var redisStore       = require('connect-redis')(expressSession);
var app              = express();
var http             = require('http').Server(app);
var io               = require('socket.io').listen(http);
var datetime = require('node-datetime');



app.use(express.static('public'));
app.use(express.static('bower_components'));
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


app.use(expressSession({
	secret: 'secrettext',
        store: new redisStore({host: '127.0.0.1',port: 6379}),
	resave: false,
	saveUninitialized: false
 } ));
 
 

app.use(function(req,res,next){
     if(!req.session){
         console.log('no session');
         return next(new Error(' some error msg'));
     }
     next();
});
 
 
var routes = require('./src/server/server-routes')(app,passport);

var server = http.listen(server_port, function () {
  console.log("Example app listening at %s",server_port);
});

db = require('./config/db.js');
io.on('connection', function (socket) {
  console.log('a user is connected');

    socket.on('chat', function (chat) {      
    var currentUserId = chat.from_id;
    var to_emp_id     = chat.to_id;
    var messages      = chat.messages;
  
     
    var dt = datetime.create();
    var fomratted = dt.format('Y-m-d H:M:S');
    
    var insertMessage = "INSERT INTO employee_chat (from_id,to_id,messages,in_date)" +
        "VALUES (" +
        "'" + currentUserId + "' ," +
        "'" + to_emp_id + "'," +
         "'" + messages + "'," +
        "'" + fomratted + "')";

        console.log(insertMessage);


      db.query(insertMessage, function (err, rows) {
        if (err) throw err;
        else {
          var data = {message: 'sucess'};
          io.emit("messageGet", {message: 'success'});
        }
      });
    });

socket.on('setStatusOnline', function (setStatusOnline) {
    var currentUserId = setStatusOnline.currentUserId;   
    console.log(setStatusOnline,'setStatusOnline is called');
    var query = "UPDATE employee_login_master SET is_online='active' where id  = '"+currentUserId+"'" ;
        db.query(query, function (err, rows) {
          if (err) throw err;
          else {
            var data = {message: 'sucess'};
            console.log('updatwd to set active');
            io.emit("getOnlineusers", {message: 'success'});
          }
        });
  });

socket.on('setStatusOffline', function (setStatusOffline) {
    var currentUserId = setStatusOffline.currentUserId;   
    console.log(setStatusOffline,'setStatusOnline is called')
    var query = "UPDATE employee_login_master SET is_online='inactive' where id  = '"+currentUserId+"'" ;
        db.query(query, function (err, rows) {
          if (err) throw err;
          else {
            var data = {message: 'sucess'};
            console.log('updatwd to set Inactive');
            io.emit("getOnlineusers", {message: 'success'});
          }
        });
  });

});

